program OB11;
Uses crt,graph;

Type
  airplane = object
    x1 ,y1 ,v1 :integer;
    procedure Coord(Var x,y :integer);virtual;
    procedure MoveAfter (Var x ,y ,v :integer);
    constructor INIT;
  private
    procedure PaintAir ( x ,y :integer);
  end;

  Bullet = object(airplane)
    Al :integer;
    procedure Coord(Var x,y:integer);virtual;
    procedure MoveAfter (Var x ,y ,v ,cos2 ,sin2 :integer);
  private
    procedure PaintBullet ( x ,y :integer);
  public
    function Heat(Var xB, yB, xA, yA :integer):boolean;
  end;

  Gun = object
    cos1 ,sin1 :integer;
    function AlphaCos ( x ,y ,v : integer): integer;
    function AlphaSin ( x,y :integer):integer;
  private
    procedure PaintGun;
  end;

constructor airplane.INIT;
begin
end;

procedure airplane.Coord(Var x,y :integer);
    Begin
      randomize;
      x := 10;
      y :=150 + random(150) mod 100;
      airplane.PaintAir(x,y);
    end;

procedure airplane.PaintAir( x ,y :integer);
  Begin
  setFillstyle(1,yellow);
  bar(x,y,x+30,y-15);
  {floodfill(x+10,y-10,Black);}
  end;

procedure airplane.MoveAfter(Var x ,y ,v :integer);
    Begin
      x := x + v;
      airplane.PaintAir( x,y);
    end;

procedure  Bullet.MoveAfter(Var x ,y ,v ,cos2 ,sin2:integer);
  Begin
    x := x + sin2;
    y := y + cos2;

    Bullet.PaintBullet(x,y);
  end;

procedure  Bullet.PaintBullet(x ,y :integer);
  Begin
    setFillStyle(1,13);
    FillEllipse(y,x,10,10);
  end;


function Bullet.Heat(Var xB, yB, xA, yA :integer):boolean;
Begin

if (yB >= yA) and ( yB  <= yA + 15) then
  if (xB >= xA) and ( xB  <= xA + 30) then begin
    Heat := true;
  end
else begin
  Heat := false;
  end;
end;


function Gun.AlphaCos(x ,y ,v :integer):integer;
  Var
    al :real;
  Begin
    al := x/4 - 10 + v;
    AlphaCos := round(al);
  end;

function Gun.AlphaSin(x ,y :integer):integer;
  Var
    al :real;
  Begin
    al := y/4 - 10;
    AlphaSin := round(al);
  end;


procedure  bullet.Coord(Var x,y:integer);
begin
x := 40;
y := 40;
end;

procedure Gun.PaintGun;
Begin
setFillstyle(1,red);
bar(0,0,40,40);
end;

{----------------------------------------------------------------------}
Var
airplaneOBJ :airplane;
bulletOBJ :Bullet;
gunOBJ :gun;
gm,gd:integer;
path :string;
Begin
  airplaneOBJ.INIT;
  bulletOBJ.INIT;
  gm := detect;
  writeln('Vvedite skorost samoleta');
  read(airplaneOBJ.v1);
  gd := detect;
  gm := 0;
  path := 'c:\bgi';
  Initgraph(gm,gd,path);
  setgraphmode(gm);
  setbkcolor(LightGray);
  gunOBJ.PaintGun;
  airplaneOBJ.Coord(airplaneOBJ.x1,airplaneOBJ.y1);

  repeat;
    setbkcolor(LightGray);
    airplaneOBJ.MoveAfter(airplaneOBJ.x1,airplaneOBJ.y1, airplaneOBJ.v1);
    gunOBJ.PaintGun;
    delay(1000);
    ClrScr;
  until KeyPressed;

  gunOBJ.cos1 := gunOBJ.AlphaCos(airplaneOBJ.x1,airplaneOBJ.y1,airplaneOBJ.v1);
  gunOBJ.sin1 := gunOBJ.AlphaSin(airplaneOBJ.x1,airplaneOBJ.y1);
  bulletOBJ.Coord(bulletOBJ.y1,bulletOBJ.x1);

  repeat

    setbkcolor(LightGray);
    airplaneOBJ.MoveAfter(airplaneOBJ.x1,airplaneOBJ.y1, airplaneOBJ.v1);
    gunOBJ.PaintGun;
    bulletOBJ.MoveAfter( bulletOBJ.y1,bulletOBJ.x1,airplaneOBJ.v1,gunOBJ.cos1,gunOBJ.sin1);
    delay(1000);
    ClrScr;
  until  bulletOBJ.Heat(bulletOBJ.x1,bulletOBJ.y1,airplaneOBJ.x1,airplaneOBJ.y1) =  true;

end.