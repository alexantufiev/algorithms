program lab10;


type
  next = ^zap;
  zap = record
    fam :string[10];
    sled_l :next;
    sled_r :next;
  end;

  next1 = ^zap1;
  zap1 = record
    fam :string[10];
    sled :next1;
  end;
  t = text;

Var
  inpatt,outpat :t;
  head :next;
  list,prevLIST :next1;


procedure koren(Var fileIN :t;Var n :next);
  begin
    new(n);
    readln(fileIN, n^.fam);
    n^.sled_l := nil;
    n^.sled_r := nil;
  end;

procedure tree(Var fileIN :t;Var n :next);
Var
  t,y :next;

Begin
  While not EOF(fileIN) do begin
    new(y);
    readln(fileIN,y^.fam);
    t := n;
    while (t^.sled_l <> y) and (t^.sled_r <> y) do begin
      if t^.fam < y^.fam then begin
        if t^.sled_r = nil then begin
          t^.sled_r := y;
          y^.sled_l := nil;
          y^.sled_r := nil;
        end
        else t := t^.sled_r;
      end
      else begin
        if t^.fam >= y^.fam then begin
          if t^.sled_l = nil then begin
            t^.sled_l := y;
            y^.sled_l := nil;
            y^.sled_r := nil;
          end
          else t := t^.sled_l;
        end;
      end;
    end;
  end;
end;


procedure Vivod_Tree(Var fileOUT :t; n :next);
Var
y,t :next;
Begin
  if n^.sled_l <> nil then
    Vivod_tree(fileOUT,n^.sled_l);

  writeln(fileOUT, n^.fam);

  if n^.sled_r <> nil then
    Vivod_tree(fileOUT, n^.sled_r);
end;


procedure Create_List(n :next;Var spisok :next1);
Var
  t,y :next1;
Begin
  if n^.sled_r <> nil then begin
    new(y);
    y^.sled := spisok;
    spisok := y;
    Create_List(n^.sled_r, spisok^.sled);
  end;
  spisok^.fam := n^.fam;
  if n^.sled_l <> nil then begin
    new(t);
    t^.sled := spisok;
    spisok := t;
    Create_List(n^.sled_l, spisok);
  end;
end;

Begin
assign(inpatt,'fams.txt');
assign(outpat,'output.txt');
reset(inpatt);
rewrite(outpat);
koren(inpatt, head);
tree(inpatt, head);
Vivod_Tree(outpat, head);
new(list);
Create_List(head, list);
writeln(outpat,'Spisok :');
While list^.sled^.sled <> nil  do begin
  writeln(outpat, list^.fam);
  list := list^.sled;
end;
close(inpatt);
close(outpat);
end.